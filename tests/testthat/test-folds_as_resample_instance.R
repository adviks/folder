context("test-folds_as_resample_instance")

test_that("holdout instance", {
  folds <- structure(1:2, blocked = FALSE, stratified = FALSE,
                     fold_weights = rep(1,2))

  expect_error(folds_as_holdout_instance(folds), NA)
})

test_that("cv instance", {
  folds <- structure(1:5, blocked = FALSE, stratified = FALSE,
                     fold_weights = rep(1,5))

  expect_error(folds_as_cv_instance(folds), NA)
})
